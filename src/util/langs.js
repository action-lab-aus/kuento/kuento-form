import langmap from "langmap";
import each from "lodash/each";

export default function langArr() {
  let tmp = [];
  each(langmap, (a, k) => {
    if (k.length == 2)
      tmp.push({
        value: k,
        label: a.nativeName,
      });
  });
  return tmp;
}
